from tornado import escape
from tornado import gen
from tornado import httpclient
from tornado import httputil
from tornado import ioloop
from tornado.websocket import websocket_connect

import json
import os
import signal
import sys
import time
import traceback
import urllib2
from threading import Thread
from Queue import Queue


DEFAULT_CONNECT_TIMEOUT = 60
DEFAULT_REQUEST_TIMEOUT = 60
MAX_BODY_SIZE = 200000000
FILE_CHUNK_SIZE = 1024 * 1024


class FileBodyProducer(object):
    def __init__(self, fname):
        self.fname = fname

    @gen.coroutine
    def produce(self, write):
        with open(self.fname, 'rb') as f:
            while True:
                buf = f.read(FILE_CHUNK_SIZE)
                if buf == '':
                    break
                yield write(buf)


class JobClient(Thread):

    def __init__(self, serverUri, jobId, token, 
                 connect_timeout=DEFAULT_CONNECT_TIMEOUT,
                 request_timeout=DEFAULT_REQUEST_TIMEOUT):
        Thread.__init__(self)
        self.serverUri = serverUri
        self.jobId = jobId
        self.token = token
        self.connect_timeout = connect_timeout
        self.request_timeout = request_timeout
        self.httpClient = httpclient.AsyncHTTPClient(max_buffer_size=MAX_BODY_SIZE)
        self.ioloop = ioloop.IOLoop.instance()
        self._ws_connection = None

    def run(self):
        try:
            self.connect()
            self.ioloop.handle_callback_exception = self.handleException
            self.ioloop.start()
        except Exception as e:
            print(e)
            self.close()

    def connect(self):
        connectUri = self.serverUri.replace('http://', 'ws://').replace('https://', 'wss://') + \
                     '/api/jobs/' + self.jobId + '/socket/raw/v1'
        headers = httputil.HTTPHeaders({
            'Authorization' : 'EverestClientToken %s' % self.token,
            'Sec-WebSocket-Protocol' : 'v1.raw-job-client.everest'
            })
        request = httpclient.HTTPRequest(url=connectUri,
                                         connect_timeout=self.connect_timeout,
                                         request_timeout=self.request_timeout,
                                         headers=headers)
        ws_conn = websocket_connect(request)
        ws_conn.add_done_callback(self._connect_callback)

    def send(self, data):
        if not self._ws_connection:
            raise RuntimeError('Web socket connection is closed.')
        self._ws_connection.write_message(escape.json_encode(data))

    def sendMessage(self, msgType, *data):
        self.send([msgType] + list(data))

    def close(self):
        if self._ws_connection is not None:
            self._ws_connection.close()
        self.ioloop.add_callback(self.ioloop.stop)

    def _connect_callback(self, future):
        if future.exception() is None:
            self._ws_connection = future.result()
            self.onConnectionSuccess()
            self._read_messages()
        else:
            self.ioloop.stop()
            self.onConnectionError(future.exception())

    @gen.coroutine
    def _read_messages(self):
        while True:
            msg = yield self._ws_connection.read_message()
            if msg is None:
                self.onConnectionClose()
                break

            msg = escape.json_decode(msg)
            self.onMessage(msg)

            msgType = msg[0]
            if msgType == 'TASK_STATE':
                taskId = msg[1]
                taskState = msg[2]
                taskInfo = msg[3]
                if taskState in ['DONE', 'FAILED', 'CANCELED']:
                    self.onTaskFinished(taskId, taskState, taskInfo)


    def onMessage(self, msg):
        print(msg)

    def onConnectionSuccess(self):
        print('Connected!')

    def onConnectionClose(self):
        print('Connection closed!')

    def onConnectionError(self, exception):
        print('Connection error: %s', exception)

    def onTaskFinished(self, taskId, taskState, taskInfo):
        pass

    def handleException(self, callback):
        traceback.print_exc()
        self.close()
        self.ioloop.stop()

    def submitTask(self, taskId, taskSpec, taskContext = {}):
        self.sendMessage('TASK_SUBMIT', taskId, taskSpec, taskContext)        

    def uploadFileAsync(self, path):
        fileName = os.path.basename(path)
        uploadUri = self.serverUri + '/api/files/jobs/%s/%s' % (self.jobId, fileName)
        headers = {
            'Authorization' : 'EverestClientToken %s' % self.token,
            'Content-Length' : str(os.path.getsize(path))
        }
        producer = FileBodyProducer(path)
        return self.httpClient.fetch(uploadUri, method='PUT', headers=headers, body_producer=producer.produce)

    def uploadFile(self, path):
        resp = self.ioloop.run_sync(lambda: self.uploadFileAsync(path))
        fileUri = resp.headers['Location']
        return fileUri

    def downloadFile(self, fileUri, file):
        headers = {
            'Authorization' : 'EverestClientToken %s' % self.token
        }
        return self.httpClient.fetch(fileUri, streaming_callback=file.write, headers=headers)


class TestClient(JobClient):

    @gen.coroutine
    def onConnectionSuccess(self):
        with open('test.txt', 'w') as f:
            f.write('some data...')
        resp = yield self.uploadFileAsync('test.txt')
        fileUri = resp.headers['Location']
        print fileUri

        self.tasks = {}
        for i in xrange(1,2):
            taskId = str(i)
            self.tasks[taskId] = {
                "command" : "sed -i '$ a\%s' test.txt" % ("task" + taskId),
                "name" : "append" + taskId,
                "inputData" : [
                    {'uri': fileUri, 'path': 'test.txt'}
                ],
                "outputData" : [{
                    "paths": ['test.txt','stdout','stderr']
                }]}


        print('Connected!')
        self.sendMessage('HELLO', {})
        self.submitTask('1', self.tasks['1'])

    @gen.coroutine
    def onTaskFinished(self, taskId, taskState, taskInfo):
        if taskState == 'DONE':
            for out in taskInfo['outputData']:
                with open(out['path'], 'wb') as f:
                    yield self.downloadFile(out['uri'], f)
        self.sendMessage('TASK_DELETE', taskId)
        self.tasks.pop(taskId)
        if len(self.tasks) > 0:
            nextTaskId = str(int(taskId) + 1)
            self.submitTask(nextTaskId, self.tasks[nextTaskId])
        else:
            self.close()


class InteractiveClient(JobClient):

    def onConnectionSuccess(self):
        print('Connected!')
        self.sendMessage('HELLO', {})

    def onMessage(self, msg):
        print(msg)
        input = raw_input('--> ')
        if input != "":
            self.send(json.loads(input))


class BlockingClient(JobClient):

    def __init__(self, serverUri, jobId, token, 
                 connect_timeout=DEFAULT_CONNECT_TIMEOUT,
                 request_timeout=DEFAULT_REQUEST_TIMEOUT):
        super(BlockingClient, self).__init__(serverUri, jobId, 
                token, connect_timeout, request_timeout)
        self.queue = Queue()

    def onConnectionSuccess(self):
        print('Connected!')
        self.sendMessage('HELLO', {})

    def onMessage(self, msg):
        print(str(msg) + '\n')
        self.queue.put(msg)

    def send(self, data):
        print('--> ' + str(data) + '\n')
        super(BlockingClient, self).send(data)

    def receive(self):
        return self.queue.get(True, 100)


def main():
    if len(sys.argv) != 3:
        print >> sys.stderr, "Usage: python job-client.py <server-uri> <resource-id>"
        exit(-1)
    else:
        serverUri = sys.argv[1]
        resourceId = sys.argv[2]

    if 'EVEREST_JOB_ID'  in os.environ and 'EVEREST_TASK_TOKEN' in os.environ:
        # running as Everest task, read jobId and token from environment
        mode = 'task'
        jobId = os.environ['EVEREST_JOB_ID']
        token = os.environ['EVEREST_TASK_TOKEN']
        print('Running as part of job: ' + jobId)
    else:
        # running as a standalone app, read token from file and create raw job
        mode = 'standalone'
        with open('.token') as f:
            token = f.read().strip()        
        req = urllib2.Request(serverUri + '/api/jobs')
        req.add_header('Content-Type', 'application/json')
        req.add_header('Authorization', 'EverestClientToken ' + token)
        resp = urllib2.urlopen(req, json.dumps({"name": "Test Raw Job"}))
        job = json.loads(resp.read())
        jobId = job['id']
        print('Created raw job: ' + jobId)

    #client = TestClient(serverUri, jobId, token)
    #client = InteractiveClient(serverUri, jobId, token)
    client = BlockingClient(serverUri, jobId, token)

    def sigHandler(sig, frame):
        client.close()
        sys.exit(0)
    signal.signal(signal.SIGINT, sigHandler)
    signal.signal(signal.SIGTERM, sigHandler)

    # upload to Everest test file
    with open('test.txt', 'w') as f:
        f.write('some data...')
    fileUri = client.uploadFile('test.txt')
    print('Uploaded file: ' + fileUri)
    
    client.start()

    client.receive()
    client.send(["TASK_SUBMIT", "1", 
                  {
                    "command":"sed -i '$ a\\task1' test.txt", 
                    "name": "append1", 
                    "outputData": [{"paths": ["test.txt","stdout","stderr"]}], 
                    "inputData": [{"path": "test.txt", "uri": fileUri}]
                  }, 
                  {}, {"resources": [resourceId]}
                ])
    while client.receive()[2] not in ['DONE','FAILED','CANCELED']:
        pass
    client.send(["TASK_SUBMIT", "2", 
                  {
                    "command": "sleep 600", 
                    "name": "Test", 
                    "inputData": [], 
                    "outputData": []
                  },
                  {},
                  {"resources": [resourceId]}
                ])
    while client.receive()[2] not in ['RUNNING']:
        pass
    client.send(["LISTDIR", "2"])
    client.receive()
    client.send(["STAGEIN", "2", [{"path": "file.txt", "uri": fileUri}, {"path": "file2.txt", "uri": "http://www.gutenberg.org/cache/epub/76/pg76.txt"}]])
    client.receive()
    client.send(["LISTDIR", "2"])
    client.receive()
    client.send(["STAGEOUT", "2", [{"paths": ["file.txt","file2.txt"], "pack": "files.tar.gz"}, {"paths": ["stdout", "stderr"]}]])
    stageOutResp = client.receive()
    archiveUri = stageOutResp[3][0]['uri']
    client.send(["TASK_SUBMIT", "3", 
                  {
                    "command": "sleep 10", 
                    "name": "Test", 
                    "inputData": [], 
                    "outputData": [{"paths": ["data/file.txt"]}]
                  }, 
                  {},
                  {"resources": [resourceId]}
                ])
    while client.receive()[2] not in ['RUNNING']:
        pass
    client.send(["STAGEIN", "3", [{"path": "files.tar.gz", "uri": archiveUri, "unpack": "data"}]])
    client.receive()
    client.send(["LISTDIR", "3"])
    client.receive()
    while client.receive()[2] not in ['DONE','FAILED','CANCELED']:
        pass
    client.send(["TASK_CANCEL", "2"])
    client.receive()

    client.close()

    while client.isAlive():
        time.sleep(1)
    client.join()

    if mode == 'standalone':
        # cancel raw job
        req = urllib2.Request(serverUri + '/api/jobs/' + jobId + '/cancel')
        req.add_header('Content-Type', 'application/json')
        req.add_header('Authorization', 'EverestClientToken ' + token)
        urllib2.urlopen(req, json.dumps({}))
        print('Canceled raw job: ' + jobId)

if __name__ == '__main__':
    main()